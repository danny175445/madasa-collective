export const whoWeAre = [
  "We are a freelance team of designers and developers who work with clients to create digital experiences that are affordable, and exactly what the client wants.",
];

export const cards = [
  {
    icon: "/product-design.png",
    title: "Product Design",
    text: "We meticulously structure the website layout to optimize user experience",
  },
  {
    icon: "/development-design.png",
    title: "Development",
    text: "We meticulously structure the website layout to optimize user experience",
  },
  {
    icon: "/branding-design.png",
    title: "Branding",
    text: "We meticulously structure the website layout to optimize user experience",
  },
];
